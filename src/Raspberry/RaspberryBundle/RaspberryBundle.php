<?php

namespace Raspberry\RaspberryBundle;

use Raspberry\RaspberryBundle\DependencyInjection\Compiler\SensorsCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class RaspberryBundle extends Bundle {

    public function build(ContainerBuilder $container) {
        $container->addCompilerPass(new SensorsCompilerPass());
    }
}
