<?php

namespace Raspberry\RaspberryBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class RaspberryExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $config, ContainerBuilder $container)
    {
        $_oLoader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $_oLoader->load('services.xml');
        $_oLoader->load('sensors.xml');
    }
}
