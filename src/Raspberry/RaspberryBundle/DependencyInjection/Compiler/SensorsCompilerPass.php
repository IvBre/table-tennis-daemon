<?php

namespace Raspberry\RaspberryBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class SensorsCompilerPass implements CompilerPassInterface {

    public function process(ContainerBuilder $container)
    {
        if (!$container->has('raspberry.provider.sensor')) {
            return;
        }

        $sensorProvider = $container->findDefinition('raspberry.provider.sensor');

        // find all service IDs with the raspberry.provider.sensor
        $registeredSensors = $container->findTaggedServiceIds('raspberry.sensor');

        foreach ($registeredSensors as $id => $tags) {
            $sensorProvider->addMethodCall('addSensor', array(new Reference($id)));
        }
    }
}
