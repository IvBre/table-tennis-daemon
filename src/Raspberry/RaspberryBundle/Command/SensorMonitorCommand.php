<?php
/**
 * @author aferracuti <adriano.ferracuti@trivago.com>
 * @since 2015-10-28
 */

namespace Raspberry\RaspberryBundle\Command;

use Raspberry\BusinessCase\MonitoringBusinessCase;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SensorMonitorCommand extends ContainerAwareCommand {
    /** @inheritdoc */
    protected function configure() {
        $this
            ->setName('sensors:monitor')
            ->setDescription('Monitor the value of registered sensors and send response to the API');
    }

    /** @inheritdoc */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $output->writeln('Sensors monitor command: started');

        /** @var MonitoringBusinessCase $businessCase */
        $businessCase = $this->getContainer()->get('raspberry.business_case.monitoring');

        $proceed = true;

        while ($proceed) {
            $sensors = $businessCase->execute();

            if (count($sensors) === 0) {
                $output->writeln('No sensors defined');
                $proceed = false;
            }

            foreach ($sensors as $id => $value) {
                $output->writeln('ID: ' . $id . ': ' . $value);
            }
        }

        $output->writeln('Sensors monitor command: ended');
    }

}
