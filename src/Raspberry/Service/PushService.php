<?php
/**
 * @author aferracuti <adriano.ferracuti@trivago.com>
 * @since 2015-10-28
 */

namespace Raspberry\Service;

class PushService implements PushServiceInterface {

    /** @var string */
    private $apiEndpoint;

    /** @var string */
    private $apiKey;

    /**
     * @param string $apiEndpoint
     * @param string $apiKey
     */
    public function __construct(
        $apiEndpoint,
        $apiKey
    ) {
        $this->apiEndpoint = $apiEndpoint;
        $this->apiKey = $apiKey;
    }

    public function send($payload) {
        $dataString = json_encode($payload);

        try {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $this->apiEndpoint . '/' . $this->apiKey);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($dataString))
            );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_exec ($ch);

            curl_close ($ch);
        } catch (\Exception $e) {
            // failed but its ok we will try again
        }
    }
}
