<?php
/**
 * @author aferracuti <adriano.ferracuti@trivago.com>
 * @since 2015-10-28
 */

namespace Raspberry\Service;

interface PushServiceInterface {

    public function send($payload);
}
