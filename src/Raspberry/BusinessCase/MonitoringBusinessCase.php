<?php
/**
 * @author aferracuti <adriano.ferracuti@trivago.com>
 * @since 2015-10-28
 */

namespace Raspberry\BusinessCase;

use Raspberry\Provider\SensorProvider;
use Raspberry\Service\PushServiceInterface;

class MonitoringBusinessCase implements MonitoringBusinessCaseInterface {

    /** @var PushServiceInterface */
    private $pushService;

    /** @var SensorProvider */
    private $sensorProvider;

    public function __construct(
        SensorProvider $sensorProvider,
        PushServiceInterface $pushService
    ) {
        $this->sensorProvider = $sensorProvider;
        $this->pushService = $pushService;
    }

    /** @inheritdoc */
    public function execute() {
        $sensors = $this->sensorProvider->getSensors();

        $pushData = [];

        foreach ($sensors as $sensor) {
            $id = $sensor->getId();
            $value = $sensor->process();
            $pushData[$id] = $value;
        }

        $this->pushService->send($pushData);

        return $pushData;
    }
}
