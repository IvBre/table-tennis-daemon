<?php
/**
 * @author aferracuti <adriano.ferracuti@trivago.com>
 * @since 2015-10-28
 */

namespace Raspberry\BusinessCase;

interface MonitoringBusinessCaseInterface {

    /** @return array */
    public function execute();
}
