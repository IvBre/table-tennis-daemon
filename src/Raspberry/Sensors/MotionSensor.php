<?php
/**
 * @author aferracuti <adriano.ferracuti@trivago.com>
 * @since  2015-10-19
 */

namespace Raspberry\Sensors;

use PhpGpio\Gpio;

class MotionSensor implements SensorInterface {
    const UNAVAILABLE_BLINKS_COUNT = 2;
    const AVAILABLE_BLINKS_COUNT = 3;
    const ORANGE_LED_PIN = 14;
    const PIR_SENSOR_PIN = 15;

    // number of checks for deciding if available
    const CHECKS = 60;

    // minimum ratio of detected movements
    const MIN_MOVEMENTS_RATIO = 0.2;

    const CHECK_INTERVAL = 1000000; // 1000 ms
    const BLINKING_INTERVAL = 200000; // 200 ms

    /** @var Gpio */
    private $gpio;

    public function __construct(Gpio $gpio) {
        $this->gpio = $gpio;
        $this->gpio->setup(self::ORANGE_LED_PIN, 'out');
        $this->gpio->setup(self::PIR_SENSOR_PIN, 'in');
    }

    /** @inheritdoc */
    public function getId() {
        return 'health-room-motion-sensor';
    }

    /** @return bool */
    public function process() {
        $movements = 0;
        for ($i = 0; $i < self::CHECKS; $i++) {
            if ($i > 0) {
                usleep(self::CHECK_INTERVAL);
            }

            $movementDetected = (int)$this->gpio->input(self::PIR_SENSOR_PIN);

            if ($movementDetected) {
                $this->outputOrangeLed(1);
                $movements++;
            }
            else {
                $this->outputOrangeLed(0);
            }
        }

        if (($movements / self::CHECKS) >= self::MIN_MOVEMENTS_RATIO) {
            $this->blinkOrangeLed(self::UNAVAILABLE_BLINKS_COUNT);

            return 0;
        }

        $this->blinkOrangeLed(self::AVAILABLE_BLINKS_COUNT);

        return 1;
    }

    private function outputOrangeLed($value) {
        $this->gpio->output(self::ORANGE_LED_PIN, $value);
    }

    private function blinkOrangeLed($times) {
        $this->outputOrangeLed(0);

        usleep(self::BLINKING_INTERVAL * 2);

        for ($i = 0; $i < $times; $i++) {
            $this->outputOrangeLed(1);
            usleep(self::BLINKING_INTERVAL);
            $this->outputOrangeLed(0);
        }
    }
}
