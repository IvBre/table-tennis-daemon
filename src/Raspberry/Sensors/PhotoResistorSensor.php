<?php

namespace Raspberry\Sensors;

use PhpGpio\Gpio;

class PhotoResistorSensor implements SensorInterface {
    const LED_PIN = 25;
    const SENSOR_PIN = 24;

    /** @var Gpio */
    private $gpio;

    public function __construct(Gpio $gpio) {
        $this->gpio = $gpio;
        $this->gpio->setup(self::LED_PIN, 'out');
        $this->gpio->setup(self::SENSOR_PIN, 'in');
    }

    /** @inheritdoc */
    public function getId() {
        return 'photo-resistor-sensor';
    }

    /** @return int */
    public function process() {
        // tired? go to sleep for a bit...
        sleep(60);

        $result = (int)$this->gpio->input(self::SENSOR_PIN);

        // 1 - available; 0 - occupied
        $result = $result === 1 ? 0 : 1;

        $this->output($result);

        return $result;
    }

    /** @param int $result */
    private function output($result) {
        $this->gpio->output(self::LED_PIN, $result);
    }
}
