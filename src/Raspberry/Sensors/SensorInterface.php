<?php

namespace Raspberry\Sensors;

interface SensorInterface {

    /** @return string */
    public function getId();

    /** @return int */
    public function process();
}
