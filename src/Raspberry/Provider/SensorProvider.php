<?php

namespace Raspberry\Provider;

use Raspberry\Sensors\SensorInterface;

class SensorProvider {

    /** @var SensorInterface[] */
    private $sensors = [];

    /** @param SensorInterface $sensor */
    public function addSensor(SensorInterface $sensor) {
        $this->sensors[] = $sensor;
    }

    /** @return SensorInterface[] */
    public function getSensors() {
        return $this->sensors;
    }
}
