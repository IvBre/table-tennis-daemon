<?php
include __DIR__ . '/../app/autoload.php';

// The functional test need a lot of memory 128M are not enough. ~mmueller
ini_set('memory_limit', '2048m');
