<?php
/**
 * @author aferracuti <adriano.ferracuti@trivago.com>
 * @since 2015-10-21
 */

namespace Raspberry\Integration;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractIntegrationTest extends WebTestCase
{
    /**
     * @var ContainerInterface
     */
    protected $oContainer;

    protected function getContainer()
    {
        if ($this->oContainer === null)
        {
            $_oClient = $this->createClient(
                [
                    'environment' => 'test'
                ]
            );
            $this->oContainer = $_oClient->getContainer();
        }

        return $this->oContainer;
    }
}
