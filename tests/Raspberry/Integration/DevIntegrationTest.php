<?php
/**
 * @author aferracuti <adriano.ferracuti@trivago.com>
 * @since 2015-10-28
 */

namespace Raspberry\Integration;

use Raspberry\BusinessCase\MonitoringBusinessCaseInterface;

class DevIntegrationTest extends AbstractIntegrationTest
{
    public function testBlinks()
    {
//        $this->markTestIncomplete('TODO');
        /** @var MonitoringBusinessCaseInterface $_oBC */
        $_oBC = $this->getContainer()->get('raspberry.business_case.monitoring');
        $_oBC->execute();
    }
}
