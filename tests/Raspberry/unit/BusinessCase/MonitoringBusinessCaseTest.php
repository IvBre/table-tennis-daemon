<?php

namespace Raspberry\BusinessCase;

use PhpGpio\Gpio;
use Raspberry\Provider\SensorProvider;
use Raspberry\Sensors\PhotoResistorSensor;
use Raspberry\Service\PushService;

class MonitoringBusinessCaseTest extends \PHPUnit_Framework_TestCase {

    public function testSensorExecution() {

        $sensor = new PhotoResistorSensor($this->prophesize(Gpio::class)->reveal());

        $sensorProvider = new SensorProvider();
        $sensorProvider->addSensor($sensor);

        $pushService = new PushService('http://fake.api.domain.com', 'some-string');

        $businessCase = new MonitoringBusinessCase($sensorProvider, $pushService);

        $response = $businessCase->execute();

        $this->assertCount(1, $response);
    }
}
