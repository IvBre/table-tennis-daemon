<?php

namespace Raspberry\Sensors;

use PhpGpio\Gpio;
use Prophecy\Argument;

class PhotoResistorSensorTest extends \PHPUnit_Framework_TestCase {

    public function testSensorActive() {
        $gpio = $this->prophesize(Gpio::class);
        $gpio->setup(Argument::cetera())->shouldBeCalled(2);
        $gpio->input(PhotoResistorSensor::SENSOR_PIN)->willReturn('1');
        $gpio->output(PhotoResistorSensor::LED_PIN, 0)->shouldBeCalled(1);

        $sensor = new PhotoResistorSensor($gpio->reveal());

        $value = $sensor->process();

        $this->assertSame(0, $value);
    }

    public function testSensorInActive() {
        $gpio = $this->prophesize(Gpio::class);
        $gpio->setup(Argument::cetera())->shouldBeCalled(2);
        $gpio->input(PhotoResistorSensor::SENSOR_PIN)->willReturn('0');
        $gpio->output(PhotoResistorSensor::LED_PIN, 1)->shouldBeCalled(1);

        $sensor = new PhotoResistorSensor($gpio->reveal());

        $value = $sensor->process();

        $this->assertSame(1, $value);
    }
}
