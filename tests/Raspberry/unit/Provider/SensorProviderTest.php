<?php

namespace Raspberry\Provider;

use PhpGpio\Gpio;
use Raspberry\Sensors\PhotoResistorSensor;

class SensorProviderTest extends \PHPUnit_Framework_TestCase {

    public function testProvider() {
        $sensor = new PhotoResistorSensor($this->prophesize(Gpio::class)->reveal());

        $sensorProvider = new SensorProvider();
        $sensorProvider->addSensor($sensor);

        $senors = $sensorProvider->getSensors();

        $this->assertInstanceOf('Raspberry\Sensors\PhotoResistorSensor', $senors[0]);
    }
}
